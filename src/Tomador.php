<?php


namespace Thiagoprz\NotaR;


class Tomador
{
    /**
     * @var string
     */
    public $identifier;

    /**
     * @var string
     */
    public $RazaoSocial;

    /**
     * @var string
     */
    public $address_street;

    /**
     * @var string
     */
    public $address_number;

    /**
     * @var string
     */
    public $address_neighborhood;

    /**
     * @var string
     */
    public $address_town;

    /**
     * @var string
     */
    public $address_state;

    /**
     * @var string
     */
    public $postal_code;

    /**
     * Tomador constructor.
     * @param string $identifier
     * @param string $RazaoSocial
     * @param string $address_street
     * @param string $address_number
     * @param string $address_neighborhood
     * @param string $address_state
     * @param string $address_town
     * @param string $postal_code
     */
    public function __construct(string $identifier, string $RazaoSocial, string $address_street, string $address_number, string $address_neighborhood, string $address_state, string $address_town, string $postal_code)
    {
        $this->identifier = $identifier;
        $this->RazaoSocial = $RazaoSocial;
        $this->address_street = $address_street;
        $this->address_number = $address_number;
        $this->address_neighborhood = $address_neighborhood;
        $this->address_state = $address_state;
        $this->address_town = $address_town;
        $this->postal_code = $postal_code;
    }
}
