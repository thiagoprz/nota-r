<?php


namespace Thiagoprz\NotaR;


use Illuminate\Support\ServiceProvider;

class NotaRServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/nota-r.php' => config_path('nota-r.php'),
        ]);
    }
}
