<?php


namespace Thiagoprz\NotaR\Servico;

/**
 * Class Valores
 * @package Thiagoprz\NotaR\Servico
 */
class Valores
{

    /**
     * @var float
     */
    public $ValorServicos;

    /**
     * @var float
     */
    public $ValorDeducoes;

    /**
     * @var float
     */
    public $ValorPis;

    /**
     * @var float
     */
    public $ValorCofins;

    /**
     * @var float
     */
    public $ValorInss;

    /**
     * @var float
     */
    public $ValorIr;

    /**
     * @var float
     */
    public $ValorCsll;

    /**
     * @var float
     */
    public $IssRetido;

    /**
     * @var float
     */
    public $ValorIss;

    /**
     * @var float
     */
    public $ValorIssRetido;

    /**
     * @var float
     */
    public $OutrasRetencoes;

    /**
     * @var float
     */
    public $BaseCalculo;

    /**
     * @var float
     */
    public $Aliquota;

    /**
     * @var float
     */
    public $ValorLiquidoNfse;

    /**
     * @var float
     */
    public $DescontoIncondicionado;

    /**
     * @var float
     */
    public $DescontoCondicionado;

    /**
     * Valores constructor.
     * @param float $ValorServicos
     * @param float $ValorDeducoes
     * @param float $ValorPis
     * @param float $ValorCofins
     * @param float $ValorInss
     * @param float $ValorIr
     * @param float $ValorCsll
     * @param float $IssRetido
     * @param float $ValorIss
     * @param float $ValorIssRetido
     * @param float $OutrasRetencoes
     * @param float $BaseCalculo
     * @param float $Aliquota
     * @param float $ValorLiquidoNfse
     * @param float $DescontoIncondicionado
     * @param float $DescontoCondicionado
     */
    public function __construct($ValorServicos, $ValorDeducoes, $ValorPis, $ValorCofins, $ValorInss, $ValorIr, $ValorCsll, $IssRetido, $ValorIss,
                                $ValorIssRetido, $OutrasRetencoes, $BaseCalculo, $Aliquota, $ValorLiquidoNfse, $DescontoIncondicionado, $DescontoCondicionado)
    {
        $this->ValorServicos = $ValorServicos;
        $this->ValorDeducoes = $ValorDeducoes;
        $this->ValorPis = $ValorPis;
        $this->ValorCofins = $ValorCofins;
        $this->ValorInss = $ValorInss;
        $this->ValorIr = $ValorIr;
        $this->ValorCsll = $ValorCsll;
        $this->IssRetido = $IssRetido;
        $this->ValorIss = $ValorIss;
        $this->ValorIssRetido = $ValorIssRetido;
        $this->OutrasRetencoes = $OutrasRetencoes;
        $this->BaseCalculo = $BaseCalculo;
        $this->Aliquota = $Aliquota;
        $this->ValorLiquidoNfse = $ValorLiquidoNfse;
        $this->DescontoIncondicionado = $DescontoIncondicionado;
        $this->DescontoCondicionado = $DescontoCondicionado;
    }
}
