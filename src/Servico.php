<?php


namespace Thiagoprz\NotaR;


use Thiagoprz\NotaR\Servico\Valores;

/**
 * Class Servico
 * @package Thiagoprz\NotaR
 */
class Servico
{
    /**
     * @var Valores
     */
    public $Valores;

    /**
     * @var string
     */
    public $ItemListaServico;

    /**
     * @var string
     */
    public $CodigoCnae;

    /**
     * @var string
     */
    public $CodigoTributacaoMunicipio;

    /**
     * @var string
     */
    public $Discriminacao;

    /**
     * @var string
     */
    public $CodigoMunicipio;

    /**
     * Servico constructor.
     * @param Valores $Valores
     * @param $ItemListaServico
     * @param $CodigoCnae
     * @param $CodigoTributacaoMunicipio
     * @param $Discriminacao
     * @param $CodigoMunicipio
     */
    public function __construct(Valores $Valores, $ItemListaServico, $CodigoCnae, $CodigoTributacaoMunicipio, $Discriminacao, $CodigoMunicipio)
    {
        $this->Valores = $Valores;
        $this->ItemListaServico = $ItemListaServico;
        $this->CodigoCnae = $CodigoCnae;
        $this->CodigoTributacaoMunicipio = $CodigoTributacaoMunicipio;
        $this->Discriminacao = $Discriminacao;
        $this->CodigoMunicipio = $CodigoMunicipio;
    }
}
