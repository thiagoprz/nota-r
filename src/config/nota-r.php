<?php
return [
    'Cnpj' => env('NOTAR_CNPJ'),
    'InscricaoMunicipal' => env('NOTAR_INSCRICAO_MUNICIPAL'),
    'OptanteSimplesNacional' => env('NOTAR_OPTANTE_SIMPLES_NACIONAL', 2),
    'IncentivadorCultural' => env('NOTAR_INCENTIVADOR_CULTURAL', 2),
    'path' => env('NOTAR_PATH'),
];
