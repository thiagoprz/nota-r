<?php


namespace Thiagoprz\NotaR;

use \DOMDocument;
use \DOMAttr;

class NotaR
{
    /**
     * @var DOMDocument
     */
    protected $dom;

    /**
     * Tag raíz do documento
     * @var DOMElement
     */
    protected $root;

    /**
     * Número da nota
     * @var int
     */
    protected $number;

    /**
     * Tag com o lote
     * @var DOMElement
     */
    protected $LoteRps;

    /**
     * Lista de RPS
     * @var DOMElement
     */
    protected $ListaRps;

    /**
     * Tag com a lista de RPS
     * @var DomElement[]
     */
    protected $rps;

    /**
     * Tag de Cnpj (será utilizada 2x no documento)
     * @var DOMElement
     */
    protected $Cnpj;

    /**
     * Tag de InscricaoMunicipal (será utilizada 2x no documento)
     * @var DOMElement
     */
    protected $InscricaoMunicipal;

    /**
     * NotaR constructor.
     * @param string $IdLote ID do lote
     * @param int $NumeroLote Número do Lote
     */
    public function __construct(string $IdLote, int $NumeroLote)
    {
        $this->dom = new DOMDocument();
        $this->dom->encoding = 'utf-8';
        $this->dom->formatOutput = true;
        $attrXmlns = new DOMAttr('xmlns', 'http://www.abrasf.org.br/nfse.xsd');
        $this->root = $this->dom->createElement('GerarNfseEnvio');
        $this->root->setAttributeNode($attrXmlns);

        $this->LoteRps = $this->dom->createElement('LoteRps');
        $attrIdLote = new DOMAttr('Id', $IdLote);
        $attrVer = new DOMAttr('versao', '1.00');
        $this->LoteRps->setAttributeNode($attrIdLote);
        $this->LoteRps->setAttributeNode($attrVer);

        // LoteRps > NumeroLote
        $NumeroLote = $this->dom->createElement('NumeroLote', $NumeroLote);
        $this->LoteRps->appendChild($NumeroLote);

        // Limpa CPF/CNPJ sem deixar caracteres especiais
        $identifier = preg_replace('/\D/', '', config('nota-r.Cnpj'));
        // LoteRps > Cnpj
        $this->Cnpj = $this->dom->createElement('Cnpj', $identifier);
        $this->LoteRps->appendChild($this->Cnpj);

        // LoteRps > InscricaoMunicipal
        $this->InscricaoMunicipal = $this->dom->createElement('InscricaoMunicipal', config('nota-r.InscricaoMunicipal'));
        $this->LoteRps->appendChild($this->InscricaoMunicipal);

        // LoteRps > ListaRps
        $this->ListaRps = $this->dom->createElement('ListaRps');
    }

    /**
     * @param $number Número da Nota
     * @param $series Série da Nota
     * @param $type Tipo da Nota
     * @param $emissionDate Data de Emissão
     * @param $operationNature Natureza da Operação
     * @param $status Status
     */
    public function createRps($number, $series, $type, $emissionDate, $operationNature, $status, Tomador $tomador, Servico $Servico)
    {
        $this->number = $number;

        // Rps
        $Rps = $this->dom->createElement('Rps');
        $attrXmlns = new DOMAttr('xmlns', 'http://www.abrasf.org.br/nfse.xsd');
        $Rps->setAttributeNode($attrXmlns);

        // Rps > InfRps
        $InfRps = $this->dom->createElement('InfRps');
        $attrInfRpsId = new DOMAttr('Id', 'R201008092026012');
        $InfRps->setAttributeNode($attrInfRpsId);

        // Rps > InfRps > IdentificacaoRps
        $IdentificacaoRps = $this->dom->createElement('IdentificacaoRps');
        // Rps > InfRps > IdentificacaoRps > Numero
        $Numero = $this->dom->createElement('Numero', $number);
        $IdentificacaoRps->appendChild($Numero);
        // Rps > InfRps > IdentificacaoRps > Serie
        $Serie = $this->dom->createElement('Serie', $series);
        $IdentificacaoRps->appendChild($Serie);
        // Rps > InfRps > IdentificacaoRps > Tipo
        $Tipo = $this->dom->createElement('Tipo', $type);
        $IdentificacaoRps->appendChild($Tipo);
        $InfRps->appendChild($IdentificacaoRps);

        // Rps > InfRps > DataEmissao
        $DataEmissao = $this->dom->createElement('DataEmissao', $emissionDate);
        $InfRps->appendChild($DataEmissao);

        // Rps > InfRps > NaturezaOperacao
        $NaturezaOperacao = $this->dom->createElement('NaturezaOperacao', $operationNature);
        $InfRps->appendChild($NaturezaOperacao);

        // Rps > InfRps > Status
        $InfStatus = $this->dom->createElement('Status', $status);
        $InfRps->appendChild($InfStatus);

        // Rps > InfRps > Servico
        $InfRps->appendChild($this->createServico($Servico));

        // Rps > InfRps > Prestador
        $InfRps->appendChild($this->createPrestador());

        // Rps > InfRps > Tomador
        $InfRps->appendChild($this->createTomador($tomador));

        $Rps->appendChild($InfRps);
        $Rps->appendChild($this->dom->createElement('Signature'));
        $this->ListaRps->appendChild($Rps);
    }

    /**
     * @return DOMElement
     */
    protected function createPrestador()
    {
        $Prestador = $this->dom->createElement('Prestador');
        $Prestador->appendChild($this->Cnpj);
        $Prestador->appendChild($this->InscricaoMunicipal);
        return $Prestador;
    }

    /**
     * @param Tomador $tomador
     * @return DOMElement
     */
    protected function createTomador(Tomador $tomador)
    {
        // Limpa CPF/CNPJ sem deixar caracteres especiais
        $identifier = preg_replace('/\D/', '', $tomador->identifier);

        // Rps > InfRps > Tomador
        $Tomador = $this->dom->createElement('Tomador');

        // Rps > InfRps > Tomador > IdentificacaoTomador
        $IdentificacaoTomador = $this->dom->createElement('IdentificacaoTomador');

        // Rps > InfRps > Tomador > IdentificacaoTomador > CpfCnpj
        $CpfCnpj = $this->dom->createElement('CpfCnpj');

        // Rps > InfRps > Tomador > IdentificacaoTomador > CpfCnpj > Cpf|Cnpj
        $Identificador = $this->dom->createElement(strlen($identifier) > 11 ? 'Cnpj' : 'Cpf', $identifier);
        $CpfCnpj->appendChild($Identificador);

        // Rps > InfRps > Tomador > RazaoSocial
        $RazaoSocial = $this->dom->createElement('RazaoSocial', $tomador->RazaoSocial);

        // Rps > InfRps > Tomador > Endereco
        $Address = $this->dom->createElement('Endereco');
        // Rps > InfRps > Tomador > Endereco > Endereco
        $Endereco = $this->dom->createElement('Endereco', $tomador->address_street);
        $Address->appendChild($Endereco);
        // Rps > InfRps > Tomador > Endereco > Numero
        $Numero = $this->dom->createElement('Numero', $tomador->address_number);
        $Address->appendChild($Numero);
        // Rps > InfRps > Tomador > Endereco > Bairro
        $Bairro = $this->dom->createElement('Bairro', $tomador->address_neighborhood);
        $Address->appendChild($Bairro);
        // Rps > InfRps > Tomador > Endereco > CodigoMunicipio
        $CodigoMunicipio = $this->dom->createElement('CodigoMunicipio', $tomador->address_town);
        $Address->appendChild($CodigoMunicipio);
        // Rps > InfRps > Tomador > Endereco > Uf
        $Uf = $this->dom->createElement('Uf', $tomador->address_state);
        $Address->appendChild($Uf);
        // Rps > InfRps > Tomador > Endereco > Cep
        $Cep = $this->dom->createElement('Cep', $tomador->postal_code);
        $Address->appendChild($Cep);

        $IdentificacaoTomador->appendChild($CpfCnpj);
        $IdentificacaoTomador->appendChild($RazaoSocial);
        $IdentificacaoTomador->appendChild($Address);
        $Tomador->appendChild($IdentificacaoTomador);
        return $Tomador;
    }

    public function createServico(Servico $Servico)
    {
        // Rps > InfRps > Servico
        $Service = $this->dom->createElement('Servico');

        // ==================== Rps > InfRps > Servico > Valores ================================================================================
        $Valores = $this->dom->createElement('Valores');
        // Rps > InfRps > Servico > Valores > ValorServicos
        $Valores->appendChild($this->dom->createElement('ValorServicos', $Servico->Valores->ValorServicos));
        // Rps > InfRps > Servico > Valores > ValorDeducoes
        $Valores->appendChild($this->dom->createElement('ValorDeducoes', $Servico->Valores->ValorDeducoes));
        // Rps > InfRps > Servico > Valores > ValorPis
        $Valores->appendChild($this->dom->createElement('ValorPis', $Servico->Valores->ValorPis));
        // Rps > InfRps > Servico > Valores > ValorCofins
        $Valores->appendChild($this->dom->createElement('ValorCofins', $Servico->Valores->ValorCofins));
        // Rps > InfRps > Servico > Valores > ValorInss
        $Valores->appendChild($this->dom->createElement('ValorInss', $Servico->Valores->ValorInss));
        // Rps > InfRps > Servico > Valores > ValorCsll
        $Valores->appendChild($this->dom->createElement('ValorCsll', $Servico->Valores->ValorCsll));
        // Rps > InfRps > Servico > Valores > IssRetido
        $Valores->appendChild($this->dom->createElement('IssRetido', $Servico->Valores->IssRetido));
        // Rps > InfRps > Servico > Valores > ValorIss
        $Valores->appendChild($this->dom->createElement('ValorIss', $Servico->Valores->ValorIss));
        // Rps > InfRps > Servico > Valores > ValorIssRetido
        $Valores->appendChild($this->dom->createElement('ValorIssRetido', $Servico->Valores->ValorIssRetido));
        // Rps > InfRps > Servico > Valores > OutrasRetencoes
        $Valores->appendChild($this->dom->createElement('OutrasRetencoes', $Servico->Valores->OutrasRetencoes));
        // Rps > InfRps > Servico > Valores > BaseCalculo
        $Valores->appendChild($this->dom->createElement('BaseCalculo', $Servico->Valores->BaseCalculo));
        // Rps > InfRps > Servico > Valores > Aliquota
        $Valores->appendChild($this->dom->createElement('Aliquota', $Servico->Valores->Aliquota));
        // Rps > InfRps > Servico > Valores > ValorLiquidoNfse
        $Valores->appendChild($this->dom->createElement('ValorLiquidoNfse', $Servico->Valores->ValorLiquidoNfse));
        // Rps > InfRps > Servico > Valores > DescontoIncondicionado
        $Valores->appendChild($this->dom->createElement('DescontoIncondicionado', $Servico->Valores->DescontoIncondicionado));
        // Rps > InfRps > Servico > Valores > DescontoCondicionado
        $Valores->appendChild($this->dom->createElement('DescontoCondicionado', $Servico->Valores->DescontoCondicionado));
        $Service->appendChild($Valores);
        // ==================== Rps > InfRps > Servico > Valores ================================================================================

        // Rps > InfRps > Servico > ItemListaServico
        $Service->appendChild($this->dom->createElement('ItemListaServico', $Servico->ItemListaServico));
        // Rps > InfRps > Servico > CodigoCnae
        $Service->appendChild($this->dom->createElement('CodigoCnae', $Servico->CodigoCnae));
        // Rps > InfRps > Servico > CodigoTributacaoMunicipio
        $Service->appendChild($this->dom->createElement('CodigoTributacaoMunicipio', $Servico->CodigoTributacaoMunicipio));
        // Rps > InfRps > Servico > Discriminacao
        $Service->appendChild($this->dom->createElement('Discriminacao', $Servico->Discriminacao));
        // Rps > InfRps > Servico > CodigoMunicipio
        $Service->appendChild($this->dom->createElement('CodigoMunicipio', $Servico->CodigoMunicipio));
        return $Service;
    }

    /**
     * Fecha o documento e armazena no diretório configurado
     */
    public function generateXML()
    {
        // LoteRps > QuantidadeRps
        $QuantidadeRps = $this->dom->createElement('QuantidadeRps', $this->ListaRps->childNodes->length);
        $this->LoteRps->appendChild($QuantidadeRps);

        $this->LoteRps->appendChild($this->ListaRps);
        $this->root->appendChild($this->dom->createElement('Signature'));
        $this->root->appendChild($this->LoteRps);
        $this->dom->appendChild($this->root);
        $path = config('nota-r.path');
        if (!is_dir($path)) {
            mkdir($path);
        }
        $file = "$path/NFS_$this->number.XML";
        $data = $this->dom->saveXML();
        file_put_contents($file, preg_replace('/^.+\n/', '', $data));
        return $file;
    }

}
